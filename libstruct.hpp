/* Library for dynamic datastructures */

#ifndef LIBSTRUCT_H
#define LIBSTRUCT_H

template <class DataType> 
class Stack
{
    private:
        DataType *StackData;
        int Top;

        int underflow();
        int overflow();
    public:
        Stack(int size);
        ~Stack();

        int push(int Data);
        void pop();
        DataType peek();
        

};

#endif //LIBSTRUCT_H